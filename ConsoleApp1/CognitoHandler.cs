﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using Amazon.CognitoIdentityProvider;
using Microsoft.AspNetCore.Identity;
using Amazon.CognitoIdentityProvider.Model;
using Amazon;
using Microsoft.Extensions.Options;
using System.Collections.Generic;

namespace CognitoApplication
{
    public class CognitoUserManager : UserManager<CognitoUser>
    {
        private string _accessKey = "AKIAJNIA4FQ2YTSUVODQ";
        private string _secretAccessKey = "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";
        private readonly AmazonCognitoIdentityProviderClient _client; 
        private readonly string _clientId = "6g1kbaa1lnckijjl0h1e8odi63";
        private readonly string _poolId = "us-east-1_t5A0IYSsN";

        public CognitoUserManager(IUserStore<CognitoUser> store): base(store, null, null, null, null, null, null, null, null)
        {
            _client = new AmazonCognitoIdentityProviderClient(_accessKey, _secretAccessKey, RegionEndpoint.USEast1);
        }

        public override Task<bool> CheckPasswordAsync(CognitoUser user, string password)
        {
            return CheckPasswordAsync(user.UserName, password);
        }

        public async Task<bool> CheckPasswordAsync(string userName, string password)
        {
            try
            {
                var authReq = new AdminInitiateAuthRequest
                {
                    UserPoolId = _poolId,
                    ClientId = _clientId,
                    AuthFlow = AuthFlowType.ADMIN_NO_SRP_AUTH
                };
                authReq.AuthParameters.Add("USERNAME", userName);
                authReq.AuthParameters.Add("PASSWORD", password);

                AdminInitiateAuthResponse authResp = await _client.AdminInitiateAuthAsync(authReq);

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
    public class CognitoUser : IdentityUser
    {
        public string Password { get; set; }
        public UserStatusType Status { get; set; }
    }
}