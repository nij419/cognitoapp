﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace CognitoApplication
{
    class Program
    {
        static HttpClient client = new HttpClient();
        static void ShowProduct(Product product)
        {
            Console.WriteLine($"Name: {product.Name}\tPrice: " +
                $"{product.Price}\tCategory: {product.Category}");
            
        }

        static async Task<Product> GetProductAsync(string path)
        {
            Product product = null;
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                product = await response.Content.ReadAsAsync<Product>();
            }
            return product;
        }


        static void Main()
        {

            runAuth();
            RunAsync().GetAwaiter().GetResult();

        }
        static async Task runAuth()
        {
            TempCognitoHandler cognitoHandler = new TempCognitoHandler();
            try
            {
                Console.WriteLine(cognitoHandler.GetClient());
                bool temp = await cognitoHandler.CheckPasswordAsync("tempUser","Asdfgh1!");
                Console.WriteLine(temp);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        static async Task RunAsync()
        {
            // Update port # in the following line.
            Product product = new Product();
            try
            {
                // Get the product
                product = await GetProductAsync("https://2yioff5vok.execute-api.us-east-1.amazonaws.com/dev/{proxy+}");
                ShowProduct(product);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }
}
